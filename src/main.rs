#![allow(unused)]

use arrayvec::ArrayVec;
use ascii::{AsciiChar, AsciiStr, AsciiString, FromAsciiError, IntoAsciiString};
use bitfield::bitfield;
use clap::Parser;
use itertools::Itertools;
use std::collections::{HashMap, HashSet};
use std::io;
use std::io::Write;
use tui::backend::CrosstermBackend;
use tui::layout::Alignment;
use tui::style::{Color, Modifier, Style};
use tui::text::{Span, Spans};
use tui::widgets::{Block, Borders};
use tui::{terminal, Terminal};

#[derive(Parser)]
#[clap(about, version)]
struct Args {
    #[clap(subcommand)]
    action: Action,
    #[clap(
        default_value = "./res/wordle-answers-alphabetical.txt",
        long,
        parse(from_os_str)
    )]
    wordlist: std::path::PathBuf,
    #[clap(
        default_value = "./res/wordle-allowed-guesses.txt",
        long,
        parse(from_os_str)
    )]
    guesslist: std::path::PathBuf,
    #[clap(long, help = "Respect hard mode rules")]
    hard: bool,
    #[clap(
        short,
        long,
        parse(from_occurrences),
        help = "Increase output, can be repeated for more verbosity"
    )]
    verbose: usize,
}

#[derive(clap::Subcommand)]
enum Action {
    /// Interactively play to guess a random Wordle
    Play,
    /// Solve a given Wordle
    Solve {
        #[clap(help = "Word to solve for")]
        word: AsciiString,
    },
}

pub enum CharHint {
    Green, Yellow, None
}

impl From<u8> for CharHint {
    fn from( ch : u8 ) -> CharHint {
        assert!( ch & 0b11111100 == 0 );

        match ch {
            2 => CharHint::Green,
            1 => CharHint::Yellow,
            _ => CharHint::None,
        }
    }
}

/// A hint in Wordle is normally conveyed in the color formatting of the guess after it is submitted.
/// A character on green is in the correct position. A character on yellow is in the word, but incorrectly placed. A character on grey is not in the word.
/// This can be encoded in a string of the form '.y..g' where '.' indicates a character not in the word, 'y' is a character present, but incorrectly placed, and 'g' is a character correctly placed.
/// There are a finite number of strings of this format (3^5, or 243), and so we can encode the hint as a 'u8'.
/// Here the ordering is a trinary 00000..22222.


struct Game<'a> {
    solution: AsciiString,
    valid_answers: Vec<&'a AsciiStr>,
    additional_guesses: Vec<&'a AsciiStr>,
    hard: bool,
}

impl Game<'_> {
    pub fn eval_guess_against(
        &self,
        guess: &AsciiStr,
        answer: &AsciiStr,
    ) -> Result<AsciiString, String> {
        assert_eq!(answer.len(), 5);
        assert!(answer.chars().all(|ch| ('a'..='z').contains(&ch)));

        if guess.len() != 5 {
            return Err(format!("Guess '{}' is not the correct length.", guess));
        }
        if !(guess.chars().all(|ch| ('a'..='z').contains(&ch))) {
            return Err(format!("Guess '{}' contains disallowed characters.", guess));
        }
        // check if guess is a recognized option
        if !self.valid_answers.contains(&guess) && !self.additional_guesses.contains(&guess) {
            return Err(format!("Guess '{}' is not a valid guess.", guess));
        }

        if self.hard {
            // evaluate existing knowledge to see if guess conforms to hard-mode criteria
        }

        let mut eval: AsciiString = AsciiString::from_ascii(".....").unwrap();
        let mut solution_chars: AsciiString = answer.to_ascii_string();

        for (i, (gc, sc)) in guess.chars().zip(answer.chars()).enumerate() {
            if gc == sc {
                *(solution_chars.chars_mut().nth(i).unwrap()) =
                    AsciiChar::from_ascii(b'.').unwrap();
            }
        }
        for (i, (gc, sc)) in guess.chars().zip(answer.chars()).enumerate() {
            if gc == sc {
                *eval.chars_mut().nth(i).unwrap() = AsciiChar::from_ascii(b'g').unwrap();
            } else if solution_chars.chars().any(|ch| ch == gc) {
                *eval.chars_mut().nth(i).unwrap() = AsciiChar::from_ascii(b'y').unwrap();
            }
        }
        Ok(eval)
    }

    pub fn eval_guess(&self, guess: &AsciiStr) -> Result<AsciiString, String> {
        self.eval_guess_against(guess, &self.solution)
    }
}

/*
fn print_guess(guess: &AsciiStr, format: &AsciiStr) {
    for (gc, fc) in guess.chars().zip(format.chars()) {
        print!(
            "{}",
            match fc {
                AsciiChar::Dot => gc.to_string().normal(),
                AsciiChar::y => gc.to_string().black().on_yellow(),
                AsciiChar::g => gc.to_string().black().on_green(),
                _ => "".normal(),
            }
        );
    }
}
*/

/*
fn format_guess<'a>( guess:&'a AsciiStr, format: &AsciiStr ) -> tui::text::Spans<'a> {
    assert_eq!( guess.len(), 5 );
    assert_eq!( format.len(), 5 );

    tui::text::Spans::from( vec![ Span::styled( guess[0], Style::default() )] )
}
*/

fn prune_corpus(mut corpus: &mut Vec<&str>, guess: &str, solution: &str) {
    for (i, (gc, sc)) in guess.chars().zip(solution.chars()).enumerate() {
        if gc == sc {
            corpus.retain(|word| word.chars().nth(i).unwrap() == gc);
        } else if solution.contains(gc) {
            corpus.retain(|word| word.chars().nth(i).unwrap() != gc && word.contains(gc));
        } else {
            corpus.retain(|word| !word.contains(gc));
        }
    }
}

fn main() -> Result<(), std::io::Error> {
    let args = Args::parse();
    let content: AsciiString = AsciiString::from_ascii(
        std::fs::read_to_string(&args.wordlist).expect("Could not read file"),
    )
    .expect("Wordlist file contained non-ASCII characters.");
    let mut words: Vec<&AsciiStr> = content.lines().collect::<Vec<&AsciiStr>>();

    let content2: AsciiString = AsciiString::from_ascii(
        std::fs::read_to_string(&args.guesslist).expect("Could not read guesslist file"),
    )
    .expect("Guesslist file contained non-ASCII characters.");
    let mut guess_words: Vec<&AsciiStr> = content2.lines().collect::<Vec<&AsciiStr>>();

    let mut valid_guesses = words.clone();
    for guess in &guess_words {
        valid_guesses.push(guess);
    }

    println!(
        "Loaded {} possible answers and {} additional valid guesses",
        words.len(),
        guess_words.len()
    );

    let mut guesses: [AsciiString; 6];

    if let Action::Solve { word } = args.action {
        //score words
        let mut wordle = Game {
            solution: word.clone(),
            valid_answers: words.clone(),
            additional_guesses: guess_words,
            hard: args.hard,
        };

        let mut word_eval: HashMap<AsciiString, u32> = HashMap::new();
        let guess1 = &word;
        {
            let mut split: HashMap<AsciiString, u32> = HashMap::new();
            for guess in &words {
                let format = wordle.eval_guess_against(&word, guess).unwrap();
                match split.get_mut(&format) {
                    Some(count) => *count += 1,
                    None => {
                        split.insert(format, 1);
                    }
                };
            }

            let mut histogram: HashMap<u32, u32> = HashMap::new();
            for (format, count) in split {
                //print_guess( &word, &format );
                //println!( " - {} options", count );

                match histogram.get_mut(&count) {
                    Some(n) => *n += 1,
                    None => {
                        histogram.insert(count, 1);
                    }
                };
            }

            for (count, n) in histogram.iter().sorted() {
                println!("{} - x{}", count, n);
            }

            // find max
            let max = histogram.iter().map(|(_, n)| n).max().unwrap();

            println!("{} - {}", &guess1, max);
        }
    } else {
        // choose a random word

        let solution = words.get(0).expect("I have no words!").to_ascii_string();

        let mut wordle = Game {
            solution,
            valid_answers: words,
            additional_guesses: guess_words,
            hard: args.hard,
        };

        crossterm::terminal::enable_raw_mode()?;
        let mut stdout = std::io::stdout();
        crossterm::execute!(
            stdout,
            crossterm::terminal::EnterAlternateScreen,
            crossterm::event::EnableMouseCapture
        )?;
        let backend = CrosstermBackend::new(stdout);
        let mut terminal = Terminal::new(backend)?;

        terminal.draw(|f| {
            let size = f.size();
            let block = Block::default()
                .title(Span::styled(
                    "Wordletouille",
                    Style::default().add_modifier(Modifier::BOLD),
                ))
                .title_alignment(Alignment::Center)
                .borders(Borders::ALL);
            f.render_widget(block, size);
        })?;

        /* show help
        terminal.draw( |f| {
            "Guess the Wordle in six tries.

            Each guess must be a valid five-letter word. Hit the enter key to submit.

            After each guess, the color of the tiles will change to show how close your guess was to the word.

            ----

            Examples

            WEARY
            The letter W is in the word and in the correct spot.

            PILLS
            The letter I is in the word but in the wrong spot.

            VAGUE
            The letter U is not in the word in any spot."
        })?;
        */

        std::thread::sleep(std::time::Duration::from_millis(2000));

        /*
        let mut guesses = 6;
        while guesses > 0 {
            let mut line = String::new();
            print!("Enter a 5 letter word to guess ({} remain): ", guesses);
            std::io::stdout().lock().flush();
            let n_bytes = std::io::stdin().read_line(&mut line).unwrap();

            let guess = AsciiStr::from_ascii(&line).unwrap().trim();
            match wordle.eval_guess(guess) {
                Ok(format) => print_guess(guess, &wordle.solution),
                Err(msg) => {
                    println!("{}", msg);
                    continue;
                }
            }
            println!();

            if guess == wordle.solution {
                break;
            }

            guesses -= 1;
        }

        println!();
        println!("The Wordle was {}", wordle.solution);
        */

        crossterm::terminal::disable_raw_mode()?;
        crossterm::execute!(
            terminal.backend_mut(),
            crossterm::terminal::LeaveAlternateScreen,
            crossterm::event::DisableMouseCapture
        )?;
        terminal.show_cursor();
    };
    Ok(())
}
